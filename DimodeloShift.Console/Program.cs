﻿using Autofac;
using System;

namespace DimodeloShift.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = AutofacContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                //var app = scope.Resolve<IApplication>();
                //app.Run();
            }
        }
    }
}

﻿using Autofac;
using DimodeloShift.Core.Common;
using DimodeloShift.Core.Interfaces;
using DimodeloShift.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Console
{
    public class AutofacContainerConfig
    {
        public static IContainer Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ExtractService>().As<IExtractService>();
            builder.RegisterType<QueueProcessor>().As<IQueueProcessor>(); 
            builder.RegisterType<Logger>().As<ILogger>();
            return builder.Build();
        }
    }
}

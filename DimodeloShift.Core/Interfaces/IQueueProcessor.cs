﻿using DimodeloShift.Core.Models.Enums;

namespace DimodeloShift.Core.Interfaces
{
    public interface IQueueProcessor
    {
        void Execute(string connection, QueueProcessorExecutingType executingType, bool isLand = false);
    }
}

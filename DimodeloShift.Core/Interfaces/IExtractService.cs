﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Interfaces
{
    public interface IExtractService
    {
        bool Validate();
        (byte[] fileBuffer,bool isContinuing) Extract();
    }
}

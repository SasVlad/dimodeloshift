﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Interfaces
{
    public interface ILogger
    {
        void LogInformation(string message);
        void LogError(string message);
    }
}

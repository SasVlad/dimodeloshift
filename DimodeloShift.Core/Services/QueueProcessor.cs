﻿using DimodeloShift.Core.Interfaces;
using DimodeloShift.Core.Models;
using DimodeloShift.Core.Models.Enums;
using DimodeloShift.Core.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Services
{
    public class QueueProcessor : IQueueProcessor
    {
        private ILogger _logger;
        private IExtractService _extractService;
        public QueueProcessor(ILogger logger, IExtractService extractService)
        {
            _logger = logger;
            _extractService = extractService;
        }
        public void Execute(ExecuteQueueProcessorModel settings)
        {
            if (_extractService.Validate())
            {
                _logger.LogInformation("Invalid connection!");
            }

            while (true)
            {
                (byte[] fileBuffer, bool isContinuing) = _extractService.Extract();
                if (!isContinuing)
                {
                    break;
                }
            }
        }

        public void Execute(string connection, QueueProcessorExecutingType executingType, bool isLand = false)
        {
            throw new NotImplementedException();
        }
    }
}

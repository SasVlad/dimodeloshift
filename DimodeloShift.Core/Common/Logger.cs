﻿using DimodeloShift.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Common
{
    public class Logger : ILogger
    {
        public void LogInformation(string message)
        {
            Console.WriteLine(message);
        }

        public void LogError(string message)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using DimodeloShift.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Models
{
    public class ExecuteQueueProcessorModel
    {
        public string Connection { get; set; }
        public QueueProcessorConnectionType QueueProcessorConnectionType { get; set; }
        public QueueProcessorExecutingType QueueProcessorExecutingType { get; set; }
    }
}

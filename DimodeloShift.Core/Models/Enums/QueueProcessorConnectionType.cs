﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Models.Enums
{
    public enum QueueProcessorConnectionType
    {
        DelimitedFile = 0,
        OleDb = 1,
        SQLClient = 2,
        ODBC = 3
    }
}

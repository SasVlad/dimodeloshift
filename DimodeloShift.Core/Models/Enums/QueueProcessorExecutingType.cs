﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DimodeloShift.Core.Models.Enums
{
    public enum QueueProcessorExecutingType
    {
        OnlyLand = 0,
        OnlyLoad = 1,
        LandAndLoad = 2
    }
}
